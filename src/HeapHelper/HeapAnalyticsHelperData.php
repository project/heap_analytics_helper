<?php

namespace Drupal\heap_analytics_helper\HeapHelper;

use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Class HeapAnalyticsHelperData.
 *
 * Holds user entity fields data.
 */
class HeapAnalyticsHelperData {

  const DEFAULT_USER_FIELDS = [
    'uid' => 'User ID',
    'name' => 'Username',
    'mail' => 'Email',
  ];

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('cache.default')
    );
  }

  /**
   * Constructs a new HeapAnalyticsHelperData.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(AccountInterface $current_user, CacheBackendInterface $cache_backend) {
    $this->currentUser = $current_user;
    $this->cache = $cache_backend;
  }

  /**
   * Get the url.
   *
   * @return array
   *   The fields data.
   */
  public function getFieldsData() {
    $uid = $this->currentUser->id();
    $result = [];
    $cid = 'heap_api_data_' . $uid;
    if ($cache = $this->cache->get($cid)) {
      $result = $cache->data;
    }
    else {
      $user = User::load($uid);
      $config = \Drupal::config('heap_analytics_helper.settings');
      $fields = $config->get('entity_fields');
      $displayLabel = $config->get('display_label');
      foreach ($fields as $field => $value) {
        if ($value !== 0) {
          $value = $user->$field->value;
          // Exclude constant field labels.
          if ($displayLabel === 1 && (!array_key_exists($field, self::DEFAULT_USER_FIELDS))) {
            $label = $user->$field->getFieldDefinition()->getLabel();
            $result[$label] = $value;
          }
          else {
            $result[$field] = $value;
          }
        }
      }
      \Drupal::cache()->set($cid, $result, Cache::PERMANENT, $user->getCacheTags());
    }
    return $result;

  }

}
