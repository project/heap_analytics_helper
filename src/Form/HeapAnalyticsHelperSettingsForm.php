<?php

namespace Drupal\heap_analytics_helper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\heap_analytics_helper\HeapHelper\HeapAnalyticsHelperData;

/**
 * Defines a form that configures heap helper settings.
 */
class HeapAnalyticsHelperSettingsForm extends ConfigFormBase {

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    parent::__construct($config_factory);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'heap_analytics_helper_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Clearing the cache on submission of settings form.
    heap_analytics_invalidate_cache();

    $this->config('heap_analytics_helper.settings')
      ->set('entity_fields', $form_state->getValue('entity_fields'))
      ->set('display_label', $form_state->getValue('display_label'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['heap_analytics_helper.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('heap_analytics_helper.settings');

    // All user fields and ones added.
    $userFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $options = HeapAnalyticsHelperData::DEFAULT_USER_FIELDS;
    foreach ($userFields as $key => $field) {
      // Get custom fields from user entity and exclude image field.
      if ($field instanceof FieldConfig && $key !== 'user_picture') {
        $options[$key] = $field->getLabel();
      }
    }

    $form['heap_helper_settings']['entity_fields'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#title' => $this->t('Select user entity fields.'),
      '#default_value' => $config->get('entity_fields'),
      '#options' => $options,
      '#description' => $this->t('Fields data to be provided to heap analytics.'),
    ];
    $form['heap_helper_settings']['display_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display field Label'),
      '#default_value' => $config->get('display_label'),
      '#description' => $this->t('Display field label instead of machine name in heap.'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
