<?php

namespace Drupal\Tests\heap_analytics_helper\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Defines a class for testing build hooks UI.
 */
class HeapAnalyticsHelperSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'heap_analytics',
    'heap_analytics_helper',
    'system',
    'user',
    'toolbar',
  ];

  /**
   * Test build hooks UI.
   */
  public function testConfigForm() {
    $this->assertThatAnonymousUsersCannotAccessSettingsForm();

    $this->drupalLogin($this->createUser([
      'administer site configuration',
      'access administration pages',
      'access content',
      'access toolbar',
      'administer heapanalytics',
    ]));
    $this->assertSettingsFormFunctionality();
    $settings = $this->assertThatAdminCanSaveSettings();
    $this->assertThatAdminCanEditSettings($settings);
  }

  /**
   * Assert that anonymous users can't access privileged pages.
   */
  private function assertThatAnonymousUsersCannotAccessSettingsForm() {
    $this->drupalGet(Url::fromRoute('heap_analytics_helper.settings_form:'));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Assert settings form functionality.
   */
  private function assertSettingsFormFunctionality() {
    $this->drupalGet(Url::fromRoute('heap_analytics_helper.settings_form'));
    $assert = $this->assertSession();
    $assert->statusCodeEquals(200);
  }

}
