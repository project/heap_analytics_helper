/**
 * @file
 * Add user properties to heap.
 */

(function ($, Drupal, drupalSettings) {
  $(window).on('load', function () {
    var json = drupalSettings.heap_analytics_helper.heapData;
    window.heap = window.heap=window.heap||[],heap.load=function(t,e){window.heap.appid=t,window.heap.config=e;var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(t){return function(){heap.push([t].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify", "addUserProperties", "setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load(json['id']);
    var uid = json['data']['uid'];
    heap.identify(uid);
    heap.addUserProperties(json['data']);
  });
})(jQuery, Drupal, drupalSettings);