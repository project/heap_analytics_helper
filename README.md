CONTENTS OF THIS FILE
---------------------

 * Introductionn
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 This module will help in logging other user information into the HEAP. It
 extends the functionality to log user entity information in Heap through
 configuration. Even this module provides the flexibility to log entity in Heap
 using entity filed label or machine name.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/heap_analytics_helper

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/heap_analytics_helper

REQUIREMENTS
-------------------

 * Heap Analytics(https://www.drupal.org/project/heap_analytics)

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * Visit the path `/admin/config/heap-analytics/configuration` and select the
 user entity fields that you want to include in the Heap.

MAINTAINERS
-----------

Current maintainers:

 * Rahul kumar (https://drupal.org/user/2361382)
 * Munish kumar (https://www.drupal.org/user/3412429)
